/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

class AloArtikel extends PolymerElement {

  static get properties() {
    return {
      aloTitle: String,
      aloContent: String
    };
  }

  ready(){
    super.ready();
    this.aloContent = JSON.parse(this.aloContent);
  }

  static get template(){
    return html`
      <style>
        .wrapper{font-family: 'Lato';font-size:14px;padding:16px;max-width: 480px;margin:auto;}
        h1{font-size:18px;border-radius:4px;padding:6px;margin:6px;
        -webkit-box-shadow: 0 0 10px 0 #BFBFBF;
        box-shadow: 0 0 10px 0 #BFBFBF;}
        .content{border-radius:4px;padding:6px;margin:6px;
        -webkit-box-shadow: 0 0 10px 0 #BFBFBF;
        box-shadow: 0 0 10px 0 #BFBFBF;text-align:left;}
        p img{max-width:100%;height:auto;}
      </style>
      <div class="wrapper">
        <h1>[[aloTitle]]</h1>
        <p class="content" inner-h-t-m-l="[[aloContent]]"></p>
      </div>
    `;
  }
}

window.customElements.define('alo-artikel', AloArtikel);
